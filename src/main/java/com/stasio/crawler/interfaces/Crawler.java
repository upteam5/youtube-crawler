package com.stasio.crawler.interfaces;

import java.io.IOException;

public interface Crawler {
    ScanInfo scanPage(String URL) throws IOException;
}
