package com.stasio;

public final class Configuration {
    private String url;
    private String password;
    private String tags_file = "tags.txt";
    private int threads = 3;
    private int links = 500;
    private int matchs = 10000;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTags_file() {
        return tags_file;
    }

    public void setTags_file(String tags_file) {
        this.tags_file = tags_file;
    }

    public int getThreads() {
        return threads;
    }

    public void setThreads(int threads) {
        this.threads = threads;
    }

    public int getLinks() {
        return links;
    }

    public void setLinks(int links) {
        this.links = links;
    }

    public int getMatchs() {
        return matchs;
    }

    public void setMatchs(int matchs) {
        this.matchs = matchs;
    }
}
