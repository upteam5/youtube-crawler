package com.stasio.test;

import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ConfigTestMain {
    public static void main(String[] args) {
        Yaml yaml = new Yaml();
        try (InputStream in = Files.newInputStream(Paths.get("configuration.yml"))) {
            Setings config = yaml.loadAs(in, Setings.class);
            System.out.println(config.getThreads());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
