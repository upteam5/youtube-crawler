package com.stasio.test;

public final class Setings {
    private String url;
    private String password;
    private String tags_file;
    private int threads=7;
    private int links;
    private int matchs;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTags_file() {
        return tags_file;
    }

    public void setTags_file(String tags_file) {
        this.tags_file = tags_file;
    }

    public int getThreads() {
        return threads;
    }

    public void setThreads(int threads) {
        this.threads = threads;
    }

    public int getLinks() {
        return links;
    }

    public void setLinks(int links) {
        this.links = links;
    }

    public int getMatchs() {
        return matchs;
    }

    public void setMatchs(int matchs) {
        this.matchs = matchs;
    }
}
