package com.stasio;


import com.stasio.crawler.impl.CrawlerImpl;
import com.stasio.crawler.impl.CrawlerWorker;
import com.stasio.crawler.interfaces.Crawler;
import com.stasio.crawler.interfaces.ScanInfo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

@SpringBootApplication
public class Main {
    // Zbior sprawdzonych linkowa
    private static HashSet<String> links = new HashSet<>();
    private static LinkedList<String> linksToVisit = new LinkedList<>();
    public static HashSet<String> linksWithMatch = new HashSet<>();
    public static final HashSet<String> TAGS = new HashSet<>();

    public static Configuration config;

//    public static final int MAX;
//    public static final int EXPECTED_MATCH = 30000;
//    private static final int THREADS = 2;


    public static void main(String[] args) {
        getConfigurations();
        SpringApplication.run(Main.class, args);
        runCrawler();
//        test();


    }

    private static void runCrawler() {
        String URL = "https://www.youtube.com/watch?v=BpICc5dfuNg";

        String[] words = {"bitcoin", "btc"};

        fillTags();

        Crawler crawler = new CrawlerImpl();
        ExecutorService pool = Executors.newFixedThreadPool(config.getThreads());

        final long START = System.currentTimeMillis();

        try {
            ScanInfo scanInfo = crawler.scanPage(URL);
//            LinkedList<String> linksOnPage = scanInfo.getLinks();
            linksToVisit = scanInfo.getLinks();

            for (int i = 0; i < config.getThreads(); i++) {
                pool.execute(new CrawlerWorker(linksToVisit.remove(0), links, words, linksToVisit));
            }

            pool.shutdown();

            if (pool.awaitTermination(1, TimeUnit.DAYS)) {
                System.out.println("-----------------------------");
                linksWithMatch.forEach(System.out::println);

                System.out.println("\nPrzeskanowano: " + links.size() + " stron w czasie " + (System.currentTimeMillis() - START) / 1000 + "s");
                System.out.println("Znaleziono: " + linksWithMatch.size() + " trafien");
                System.out.println("Links to visit: " + linksToVisit.size());
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void fillTags() {
        String fileName = config.getTags_file();

        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach(TAGS::add);
        } catch (IOException e) {
            System.err.println("Main.fillTags() : Problem z odczytaniem tagow z pliku .txt");
        }
    }

    private static void getConfigurations() {
        Yaml yaml = new Yaml();
        try (InputStream in = Files.newInputStream(Paths.get("configuration.yml"))) {
            config = yaml.loadAs(in, Configuration.class);
//            System.out.println(config.getMatchs());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
